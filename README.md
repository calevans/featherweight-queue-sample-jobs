# Sample Queue Jobs

Cal Evans <cal@calevans.com>

This is a repo of sample jobs to show how to create jobs for Featherweight Queue.

# TODO

- Rename Sample to Simple
- Add a job that sends an email. (Mailhog should catch)
- Add a job that hits an API endpoint. Require guzzle to do it.