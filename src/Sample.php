<?php

declare(strict_types=1);

namespace Eicc\Fwq\SampleQueueJobs;

/**
 * This is the sample job.
 *
 * Everything the job needs to execute should be passed in through the
 * $parameters array. The job should not know anything about the environment it
 * is executing in.
 */
class Sample
{
  public function __invoke(array $parameters)
  {
    echo "Hello " . ($parameters['name'] ?? '') . "!\n";
  }
}
